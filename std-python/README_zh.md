## 一. 将std-python目录复制到l2工程的developtools目录下

![](imgs/img001.png)

## 二. productdefine\common\products\Hi3516DV300.json中添加std-python模块

![](imgs/img002.png)

## 三. 编译./build.sh --product-name Hi3516DV300，完成后会在out/ohos-arm-release/developtools/std-python生成python可执行文件和库文件

![](imgs/img003.png)

## 四. 运行preinstall.py脚本，自动将生成文件复制到install目录，并改名(需要在工程根目录运行)

1. sudo python developtools/std-python/preinstall.py

![](imgs/img004.png)

## 五. 将install目录打包推送到3516开发板

1. tar -cf install.tar developtools/std-python/install
2. hdc file send /home/cc/tt/l2/install.tar /data/local/tmp

![](imgs/img005.png)

## 六. 命令行运行python进入交互界面

1. tar -xf install.tar
2. cd developtools/std-python/install/
3. ./install.sh

![](imgs/img006.png)
