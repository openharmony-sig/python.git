import os
import re

libs_dir="developtools/std-python/install/lib/python3.8/lib-dynload"

os.system("cp out/ohos-arm-release/developtools/std-python/python developtools/std-python/install/bin")
os.system("cp out/ohos-arm-release/developtools/std-python/libhipython.z.so developtools/std-python/install/lib")
os.system("cp out/ohos-arm-release/developtools/std-python/libmodule*.z.so %s"%libs_dir)

for fn in os.listdir(libs_dir):
    tt=re.match("libmodule([_a-z0-9]+)\.z\.so",fn)
    if tt:
        # print(fn[tt.regs[1][0]:tt.regs[1][1]])
        os.system("mv %s/%s %s/%s.pythonlib.so"%(libs_dir,fn,libs_dir,fn[tt.regs[1][0]:tt.regs[1][1]]))

print("ok")