/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#ifndef UART_IMPL_H
#define UART_IMPL_H

#include "py/objtype.h"
#include "py/runtime.h"
#include "spi_ex.h"

class_type(spi_info, SpiInfo);

mp_obj_t mp_spi_init(mp_obj_t idx, mp_obj_t info);
mp_obj_t mp_spi_deinit(mp_obj_t idx);
mp_obj_t mp_spi_host_write(mp_obj_t idx, mp_obj_t data);
mp_obj_t mp_spi_host_read(mp_obj_t idx, mp_obj_t n);
mp_obj_t mp_spi_host_write_read(mp_obj_t idx, mp_obj_t data);
mp_obj_t mp_spi_set_info(mp_obj_t idx, mp_obj_t info);


#endif
