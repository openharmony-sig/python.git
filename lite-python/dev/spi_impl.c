/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#include <malloc.h>
#include "mperrno.h"
#include "utility.h"
#include "spi_impl.h"
#include "spi_ex.h"

extern const mp_obj_type_t class_object(spi_info);

mp_obj_t mp_spi_init(mp_obj_t idx, mp_obj_t info)
{
    int id = mp_obj_get_int(idx);
    unsigned int r = -1;
    
    if( mp_obj_is_type(info, &class_object(spi_info)) )
    {
        class_name(spi_info)* pInfo = MP_OBJ_TO_PTR(info); 
           
        r = DTSpiInit(id, pInfo->member);
    }
    else
    {
        mp_raise_TypeError(MP_ERROR_TEXT("can't convert to spi_info"));
    }
    
    return mp_obj_new_int(r);
}

mp_obj_t mp_spi_deinit(mp_obj_t idx)
{
    return mp_obj_new_int(DTSpiDeinit(mp_obj_get_int(idx)));
}

mp_obj_t mp_spi_host_write(mp_obj_t idx, mp_obj_t data)
{
    unsigned int r = -1;
    int id = mp_obj_get_int(idx);
    
    if( mp_obj_is_type(data, &mp_type_list) )
    {
        unsigned char* bytes = NULL;
        size_t len = pylist_to_integer_array(data, bytes);
        
        if( bytes )
        {       
            r = DTSpiHostWrite(id, bytes, len) >= 0 ? 0 : -1;
        }
        else
        {
            mp_raise_OSError(MP_ENOMEM);
        }
        
        free(bytes);
    }
    else
    {    
        mp_raise_TypeError(MP_ERROR_TEXT("can't convert to list"));
    }
   
    return mp_obj_new_int(r);
}

mp_obj_t mp_spi_host_read(mp_obj_t idx, mp_obj_t len)
{  
    unsigned int r = -1;
    int id = mp_obj_get_int(idx);
    size_t rlen = mp_obj_get_int(len);
    unsigned char* buffer = malloc(sizeof(*buffer) * rlen);
    mp_obj_t ret[2] = 
    {
        mp_obj_new_int(-1),
        mp_const_none,
    };
    
    if( buffer )
    {
        mp_obj_t pylist = mp_const_none;
        
        r = DTSpiHostRead(id, buffer, rlen) >= 0 ? 0 : -1;
        
        if( (r == 0) && ((pylist = integer_array_to_pylist(buffer, rlen)) != mp_const_none) )
        {           
            ret[0] = mp_obj_new_int(0);
            ret[1] = pylist;
        }
    }
    else
    {
        mp_raise_OSError(MP_ENOMEM);
    }
    
    free(buffer);
    
    return mp_obj_new_tuple(2, ret);
}

mp_obj_t mp_spi_host_write_read(mp_obj_t idx, mp_obj_t data)
{
    unsigned int r = -1;
    int id = mp_obj_get_int(idx);
    mp_obj_t ret[2] = 
    {
        mp_obj_new_int(-1),
        mp_const_none,
    };
    
    if( mp_obj_is_type(data, &mp_type_list) )
    {
        unsigned char* bytes = NULL;
        size_t n = pylist_to_integer_array(data, bytes);        
        unsigned char* buffer = malloc(sizeof(*buffer) * n);
        
        if( bytes && buffer )
        {       
            mp_obj_t pylist = mp_const_none;
        
            r = DTSpiHostWriteRead(id, bytes, buffer, n) >= 0 ? 0 : -1;
        
            if( (r == 0) && ((pylist = integer_array_to_pylist(buffer, n)) != mp_const_none) )
            {           
                ret[0] = mp_obj_new_int(0);
                ret[1] = pylist;
            }
        }
        else
        {
            mp_raise_OSError(MP_ENOMEM);
        }
        
        free(bytes);
        free(buffer);
    }
    else
    {    
        mp_raise_TypeError(MP_ERROR_TEXT("can't convert to list"));
    }
   
    return mp_obj_new_tuple(2, ret);
}

mp_obj_t mp_spi_set_info(mp_obj_t idx, mp_obj_t info)
{
    int id = mp_obj_get_int(idx);
    unsigned int r = -1;
    
    if( mp_obj_is_type(info, &class_object(spi_info)) )
    {
        class_name(spi_info)* pInfo = MP_OBJ_TO_PTR(info); 
           
        r = DTSpiSetInfo(id, pInfo->member);
    }
    else
    {
        mp_raise_TypeError(MP_ERROR_TEXT("can't convert to spi_info"));
    }
    
    return mp_obj_new_int(r);
}


