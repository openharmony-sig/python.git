/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#include <malloc.h>
#include "mperrno.h"
#include "utility.h"
#include "uart_impl.h"

extern const mp_obj_type_t class_object(uart_attr);

mp_obj_t mp_uart_init(mp_obj_t idx, mp_obj_t attr)
{
    int id = mp_obj_get_int(idx);
    unsigned int r = -1;
    
    if( mp_obj_is_type(attr, &class_object(uart_attr)) )
    {
        class_name(uart_attr)* pAttr = MP_OBJ_TO_PTR(attr); 
        
        pAttr->member.rxBlock = IOT_UART_BLOCK_STATE_BLOCK;
        pAttr->member.txBlock = IOT_UART_BLOCK_STATE_BLOCK;
        
        r = IoTUartInit(id, &pAttr->member);
    }
    else
    {
        mp_raise_TypeError(MP_ERROR_TEXT("can't convert to uart_attr"));
    }
    
    return mp_obj_new_int(r);
}

mp_obj_t mp_uart_deinit(mp_obj_t idx)
{
    return mp_obj_new_int(IoTUartDeinit(mp_obj_get_int(idx)));
}

mp_obj_t mp_uart_write(mp_obj_t idx, mp_obj_t data)
{
    unsigned int r = -1;
    int id = mp_obj_get_int(idx);
    
    if( mp_obj_is_type(data, &mp_type_list) )
    {
        unsigned char* bytes = NULL;
        size_t len = pylist_to_integer_array(data, bytes);
        
        if( bytes )
        {       
            r = IoTUartWrite(id, bytes, len) >= 0 ? 0 : -1;
        }
        else
        {
            mp_raise_OSError(MP_ENOMEM);
        }
        
        free(bytes);
    }
    else
    {    
        mp_raise_TypeError(MP_ERROR_TEXT("can't convert to list"));
    }
   
    return mp_obj_new_int(r);
}

mp_obj_t mp_uart_read(mp_obj_t idx)
{
    enum { BUFF_SIZE = 256 };
    
    unsigned int r = -1;
    int id = mp_obj_get_int(idx);
    unsigned char* buffer = malloc(sizeof(*buffer) * BUFF_SIZE);
    mp_obj_t ret[2] = 
    {
        mp_obj_new_int(-1),
        mp_const_none,
    };
    
    if( buffer )
    {
        mp_obj_t pylist = mp_const_none;
        
        r = IoTUartRead(id, buffer, BUFF_SIZE) >= 0 ? 0 : -1;
        
        if( (r == 0) && ((pylist = integer_array_to_pylist(buffer, r)) != mp_const_none) )
        {           
            ret[0] = mp_obj_new_int(0);
            ret[1] = pylist;
        }
    }
    else
    {
        mp_raise_OSError(MP_ENOMEM);
    }
    
    free(buffer);
    
    return mp_obj_new_tuple(2, ret);
}

mp_obj_t mp_uart_set_flow_ctrl(mp_obj_t idx, mp_obj_t ctrl)
{
    int id = mp_obj_get_int(idx);
    unsigned int fc = mp_obj_get_int(ctrl);  
    unsigned int r = IoTUartSetFlowCtrl(id, (IotFlowCtrl)fc);
    
    return mp_obj_new_int(r);
}
