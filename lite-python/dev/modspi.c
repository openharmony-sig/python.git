/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#include <string.h>
#include "mperrno.h"
#include "utility.h"
#include "spi_impl.h"

STATIC const type_field_info_t g_field_offset[] =
{
    type_offset_value("cpol", SpiInfo, cpol, 0, 1),
    type_offset_value("cpha", SpiInfo, cpha, 0, 1),
    type_offset_value("protocol", SpiInfo, protocol, 0, 4),
    type_offset_value("data_width", SpiInfo, dataWidth, 0, 16),
    type_offset_value("endian", SpiInfo, endian, 0, 1),
    type_offset_value("freq", SpiInfo, freq, 0, 0xFFFFFFFF),
};

class_func(spi_info, g_field_offset);

STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_spi_init_obj, mp_spi_init);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_spi_deinit_obj, mp_spi_deinit);
STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_spi_host_write_obj, mp_spi_host_write);
STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_spi_host_read_obj, mp_spi_host_read);
STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_spi_host_write_read_obj, mp_spi_host_write_read);
STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_spi_set_info_obj, mp_spi_set_info);

STATIC const mp_rom_map_elem_t mp_module_spi_globals_table[] = 
{
    {MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_spi)},    
    {MP_ROM_QSTR(MP_QSTR_spi_info), MP_ROM_PTR(&class_object(spi_info))},
    {MP_ROM_QSTR(MP_QSTR_spi_init), MP_ROM_PTR(&mp_spi_init_obj)},
    {MP_ROM_QSTR(MP_QSTR_spi_deinit), MP_ROM_PTR(&mp_spi_deinit_obj)},
    {MP_ROM_QSTR(MP_QSTR_host_write), MP_ROM_PTR(&mp_spi_host_write_obj)},
    {MP_ROM_QSTR(MP_QSTR_host_read), MP_ROM_PTR(&mp_spi_host_read_obj)},
    {MP_ROM_QSTR(MP_QSTR_host_write_read), MP_ROM_PTR(&mp_spi_host_write_read_obj)},
    {MP_ROM_QSTR(MP_QSTR_set_info), MP_ROM_PTR(&mp_spi_set_info_obj)},
};

STATIC MP_DEFINE_CONST_DICT(mp_module_spi_globals, mp_module_spi_globals_table); 

const mp_obj_module_t mp_module_spi =
{
    .base = {&mp_type_module},    
    .globals = (mp_obj_dict_t*)&mp_module_spi_globals,
};

MP_REGISTER_MODULE(MP_QSTR_spi, mp_module_spi, 1);
