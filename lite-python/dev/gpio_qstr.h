/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

Q(_gpio_begin_)
Q(gpio_init)
Q(gpio_deinit)
Q(get_dir)
Q(set_dir)
Q(get_output)
Q(set_output)
Q(get_input)
Q(set_pull)
Q(set_func)
Q(query_pull_value)
Q(query_func_value)
Q(get_isr_mode)
Q(set_isr_mode)
Q(unregister_isr_func)
Q(register_isr_func)
Q(set_isr_mask)
Q(level_low)
Q(level_high)
Q(fall_low)
Q(rise_high)
Q(dir_in)
Q(dir_out)
Q(_gpio_end_)
