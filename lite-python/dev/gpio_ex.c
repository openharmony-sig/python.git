/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#include <strings.h>
#include "utility.h"
#include "gpio_ex.h"

typedef struct
{
    const int gpio;
    key_val_t func;
} gpio_func_val_t;

static const gpio_func_val_t g_func_value[] = 
{
    
};

static const key_val_t g_pull_value[] = 
{

};

int gpio_query_func_value(unsigned int gpio, const char* func)
{
    int ret = -1;
    int i = 0;
    
    for(i=0; i<dim(g_func_value); i++)
    {
        if( (gpio == g_func_value[i].gpio) && (strcasecmp(func, g_func_value[i].func.key) == 0) )
        {
            ret = g_func_value[i].func.val;
            break;
        }
    }
    
    return ret;
}

int gpio_query_pull_value(const char* pull)
{
    return query_value(pull, g_pull_value, dim(g_pull_value));
}
