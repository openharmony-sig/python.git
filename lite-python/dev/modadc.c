/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#include "adc_impl.h"

STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_adc_set_model_obj, mp_adc_set_model);
STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_adc_set_bais_obj, mp_adc_set_bais);
STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_adc_set_reset_count_obj, mp_adc_set_reset_count);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_adc_read_obj, mp_adc_read);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_adc_query_bais_value_obj, mp_adc_query_bais_value);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_adc_query_model_value_obj, mp_adc_query_model_value);

STATIC const mp_rom_map_elem_t mp_module_adc_globals_table[] = 
{
    {MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_adc)},
    {MP_ROM_QSTR(MP_QSTR_set_model), MP_ROM_PTR(&mp_adc_set_model_obj)},
    {MP_ROM_QSTR(MP_QSTR_set_bais), MP_ROM_PTR(&mp_adc_set_bais_obj)},
    {MP_ROM_QSTR(MP_QSTR_set_reset_count), MP_ROM_PTR(&mp_adc_set_reset_count_obj)},
    {MP_ROM_QSTR(MP_QSTR_read), MP_ROM_PTR(&mp_adc_read_obj)},
    {MP_ROM_QSTR(MP_QSTR_query_bais_value), MP_ROM_PTR(&mp_adc_query_bais_value_obj)},
    {MP_ROM_QSTR(MP_QSTR_query_model_value), MP_ROM_PTR(&mp_adc_query_model_value_obj)},
};

STATIC MP_DEFINE_CONST_DICT(mp_module_adc_globals, mp_module_adc_globals_table); 

const mp_obj_module_t mp_module_adc =
{
    .base = {&mp_type_module},    
    .globals = (mp_obj_dict_t*)&mp_module_adc_globals,
};

MP_REGISTER_MODULE(MP_QSTR_adc, mp_module_adc, 1);

