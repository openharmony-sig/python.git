/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#include <string.h>
#include "mperrno.h"
#include "utility.h"
#include "uart_impl.h"

STATIC const type_field_info_t g_field_offset[] =
{
    type_offset_value("baudRate", IotUartAttribute, baudRate, 0, 0xFFFFFFFF),
    type_offset_value("dataBits", IotUartAttribute, dataBits, IOT_UART_DATA_BIT_5, IOT_UART_DATA_BIT_8),
    type_offset_value("stopBits", IotUartAttribute, stopBits, IOT_UART_STOP_BIT_1, IOT_UART_STOP_BIT_2),
    type_offset_value("parity", IotUartAttribute, parity, IOT_UART_PARITY_NONE, IOT_UART_PARITY_EVEN),
};

class_func(uart_attr, g_field_offset);

STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_uart_init_obj, mp_uart_init);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_uart_deinit_obj, mp_uart_deinit);
STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_uart_write_obj, mp_uart_write);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_uart_read_obj, mp_uart_read);
STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_uart_set_flow_ctrl_obj, mp_uart_set_flow_ctrl);

STATIC const mp_rom_map_elem_t mp_module_uart_globals_table[] = 
{
    {MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_uart)},
    {MP_ROM_QSTR(MP_QSTR_data_bit_5), MP_ROM_INT(IOT_UART_DATA_BIT_5)},
    {MP_ROM_QSTR(MP_QSTR_data_bit_6), MP_ROM_INT(IOT_UART_DATA_BIT_6)},
    {MP_ROM_QSTR(MP_QSTR_data_bit_7), MP_ROM_INT(IOT_UART_DATA_BIT_7)},
    {MP_ROM_QSTR(MP_QSTR_data_bit_8), MP_ROM_INT(IOT_UART_DATA_BIT_8)},   
    {MP_ROM_QSTR(MP_QSTR_stop_bit_1), MP_ROM_INT(IOT_UART_STOP_BIT_1)},
    {MP_ROM_QSTR(MP_QSTR_stop_bit_2), MP_ROM_INT(IOT_UART_STOP_BIT_2)},   
    {MP_ROM_QSTR(MP_QSTR_parity_none), MP_ROM_INT(IOT_UART_PARITY_NONE)},
    {MP_ROM_QSTR(MP_QSTR_parity_odd), MP_ROM_INT(IOT_UART_PARITY_ODD)},
    {MP_ROM_QSTR(MP_QSTR_parity_even), MP_ROM_INT(IOT_UART_PARITY_EVEN)},       
    {MP_ROM_QSTR(MP_QSTR_flow_ctrl_none), MP_ROM_INT(IOT_FLOW_CTRL_NONE)},
    {MP_ROM_QSTR(MP_QSTR_flow_ctrl_rts_cts), MP_ROM_INT(IOT_FLOW_CTRL_RTS_CTS)},
    {MP_ROM_QSTR(MP_QSTR_flow_ctrl_rts_only), MP_ROM_INT(IOT_FLOW_CTRL_RTS_ONLY)},
    {MP_ROM_QSTR(MP_QSTR_flow_ctrl_cts_only), MP_ROM_INT(IOT_FLOW_CTRL_CTS_ONLY)},   
    {MP_ROM_QSTR(MP_QSTR_uart_attr), MP_ROM_PTR(&class_object(uart_attr))},
    {MP_ROM_QSTR(MP_QSTR_uart_init), MP_ROM_PTR(&mp_uart_init_obj)},
    {MP_ROM_QSTR(MP_QSTR_uart_deinit), MP_ROM_PTR(&mp_uart_deinit_obj)},
    {MP_ROM_QSTR(MP_QSTR_write), MP_ROM_PTR(&mp_uart_write_obj)},
    {MP_ROM_QSTR(MP_QSTR_read), MP_ROM_PTR(&mp_uart_read_obj)},
    {MP_ROM_QSTR(MP_QSTR_set_flow_ctrl), MP_ROM_PTR(&mp_uart_set_flow_ctrl_obj)},
};

STATIC MP_DEFINE_CONST_DICT(mp_module_uart_globals, mp_module_uart_globals_table); 

const mp_obj_module_t mp_module_uart =
{
    .base = {&mp_type_module},    
    .globals = (mp_obj_dict_t*)&mp_module_uart_globals,
};

MP_REGISTER_MODULE(MP_QSTR_uart, mp_module_uart, 1);


